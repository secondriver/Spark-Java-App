package secondriver.spark;

import java.io.IOException;

import secondriver.spark.core.SparkEmitter;

/**
 * App Core Start from Here!
 */
public class App {

	public static void main(String[] args) throws IOException {
		SparkEmitter.createSparkEmitter().launch();
	}
}