package secondriver.spark.core.impl;

import secondriver.spark.core.SparkException;
import spark.Spark;

public class DefaultSparkExpcetion implements SparkException {

	public void init() {

		Spark.exception(Exception.class, (e, request, response) -> {
			response.status(404);
			response.body("Resource not found");
		});

	}

}
