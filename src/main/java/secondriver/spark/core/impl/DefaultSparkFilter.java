package secondriver.spark.core.impl;

import secondriver.spark.core.SparkFilter;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

public class DefaultSparkFilter implements SparkFilter {

	public void init() {
		this.setBefore();
		this.setAfter();
	}

	public void setBefore() {
		// Spark.before(new Filter() {
		//
		// @Override
		// public void handle(Request request, Response response)
		// throws Exception {
		// System.out.println(request.pathInfo());
		// if (request.pathInfo().startsWith("/public")) {
		//
		// } else {
		// Session session = request.session();
		// String u = session.attribute("u");
		// String p = session.attribute("p");
		// if (u != null && p != null) {
		//
		// } else {
		// response.redirect("/public/registered.html");
		// }
		// }
		// }
		// });
	}

	public void setAfter() {

		Spark.after(new Filter() {

			@Override
			public void handle(Request request, Response response)
					throws Exception {
				System.out.println("全局后置过滤器");
			}
		});

	}

}
