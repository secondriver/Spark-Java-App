package secondriver.spark.core.impl;

import java.io.File;

import secondriver.spark.core.SparkConfigure;
import spark.Spark;

public class InitSparkConfigure implements SparkConfigure {

	private int port = 9000;

	private String host = "127.0.0.1";

	private String classResources = ".";

	private String staticResources = System.getProperty("user.dir")
			+ File.separator + "/public";

	public InitSparkConfigure() {
		super();
	}

	public InitSparkConfigure(int port, String host, String classResources,
                              String staticResources) {
		super();
		this.port = port;
		this.host = host;
		this.classResources = classResources;
		this.staticResources = staticResources;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getClassResources() {
		return classResources;
	}

	public void setClassResources(String classResources) {
		this.classResources = classResources;
	}

	public String getStaticResources() {
		return staticResources;
	}

	public void setStaticResources(String staticResources) {
		this.staticResources = staticResources;
	}

	public void init() {
		Spark.setPort(port);
		Spark.setIpAddress(host);
		Spark.staticFileLocation(classResources);
		Spark.externalStaticFileLocation(staticResources);
	}
}
