package secondriver.spark.route;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

public class LoginRoute implements Route {

	@Override
	public Object handle(Request request, Response response) {
		String username = request.params("u");
		String password = request.params("p");
		System.out.println(String.format("用户名：%s,密码：%s", username, password));

		Session session = request.session();
		String u = session.attribute("u");
		String p = session.attribute("p");
		if (u != null && p != null) {
			if (u.equals(username) && p.equals(password)) {
				return "登录成功："
						+ String.format("用户名：%s,密码：%s", username, password);
			} else {
				return "用户名或密码错误";
			}
		} else {
			response.redirect("/registered.html");
			return this;
		}
	}
}
