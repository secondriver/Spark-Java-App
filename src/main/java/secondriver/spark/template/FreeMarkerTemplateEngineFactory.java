package secondriver.spark.template;

import java.io.File;
import java.io.IOException;

import freemarker.template.Configuration;
import spark.template.freemarker.FreeMarkerEngine;

public final class FreeMarkerTemplateEngineFactory {

	private static FreeMarkerEngine templateEngine;

	private static Configuration configuration = new Configuration();

	static {
		configuration.setAutoFlush(true);
		configuration.setDateFormat("yyyy-MM-dd");
		try {
			configuration.setDirectoryForTemplateLoading(new File(System
					.getProperty("user.dir") + "/private/template"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		configuration.setOutputEncoding("UTF-8");
		configuration.setDateTimeFormat("yyyy-MM-dd HH:mm:ss");
		configuration.setTimeFormat("HH:mm:ss");
	}

	private FreeMarkerTemplateEngineFactory() {

	}

	public synchronized static FreeMarkerEngine getFreeMarkerEngine() {
		if (null == templateEngine) {
			templateEngine = new FreeMarkerEngine(configuration);
		}
		return templateEngine;
	}
}