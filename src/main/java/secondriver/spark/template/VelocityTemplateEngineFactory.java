package secondriver.spark.template;

import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;

import spark.template.velocity.VelocityTemplateEngine;

public final class VelocityTemplateEngineFactory {

	private VelocityTemplateEngineFactory() {

	}

	private static VelocityTemplateEngine templateEngine;

	public static synchronized VelocityTemplateEngine getVelocityTemplateEngine() {
		if (null == templateEngine) {
			templateEngine = new VelocityTemplateEngine();
		}
		return templateEngine;
	}

	public static synchronized VelocityTemplateEngine getVelocityTemplateEngine(
			Properties properties) {
		if (null == templateEngine) {
			final VelocityEngine velocityEngine = new VelocityEngine(properties);
			templateEngine = new VelocityTemplateEngine(velocityEngine);
		}
		return templateEngine;
	}
}
